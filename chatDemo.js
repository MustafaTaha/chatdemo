import React, { Component } from 'react';

import config from 'config';
import { Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import Icon from '@material-ui/core/Icon';
import { IMG01, IMG02, IMG09, IMG010 } from './img';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import * as signalR from '@microsoft/signalr';
import Dataservice from '../../../../Dataservice';
//import Config from '../../../../Services/Config';
import 'regenerator-runtime/runtime'
import LoadingSection from '../../../../Loader/Loader';
import { Formik, Form } from 'formik';

import Avatar from '../../../assets/images/avatar.svg'

class DoctorChat extends Component {
	chatContainer = React.createRef();
	constructor(props) {
		super(props);
		this.state = {
			activeModal: null,
			patientList: [],
			selectedPatient: {},
			messagesList: [],
			chatSlide: false,
			patient: {},
			isLoading: true,
			patientsLoading: false,
			chatLoading: false,
			activeClass: false,
			chatobj: {
				subject: '',
				//isPatient:false
			}
		}
		this.onKeyUp = this.onKeyUp.bind(this);
		this.connection = new signalR.HubConnectionBuilder()
			.withUrl(`${config.hubUrl}ChatHub?user-id=${window.localStorage.getItem('accountId')}`, {
				transport: signalR.HttpTransportType.WebSockets | signalR.HttpTransportType.LongPolling,
			}).configureLogging(signalR.LogLevel.Information)
			.withAutomaticReconnect()
			.build();
	}

	openModal = (id) => {
		this.setState({ activeModal: id }, () => {
		});
	}

	handleCloseModal = () => {
		this.setState({ activeModal: false }, () => {
		});
	}

	showCoversations = () => {
		let url = parseInt(reqId) > 0 ? 'GetInfluencersTalkedClientByReqId?reqId=' + parseInt(reqId) + '&influencerId=' + this.state.influencerId : 'GetAllInfluencersTalkedClient';
		Dataservice.GetDataGrid(url).then((res) => {
			if (res) {
				this.setState({
					oldRows: res || [],
					influencersTalked: res || [],
					isLoading: false,
				});

				if (this.state.influencerId > 0) {
					let exist = res.find(
						(item) => item.id == this.state.influencerId,
					);
					if (exist > 0) {
						this.getMessages(res.indexOf(exist));
						this.scrollToBottom('messages');
					} else {
						this.getInfluencerInfo();
					}
				} else {
					if (res != undefined && res != null && res.length > 0) {
						this.getMessages(res[0]);
						this.scrollToBottom('messages');
					}
				}
			} else {
				this.getInfluencerInfo();
			}
		});
	};

	async componentDidMount() {

		Dataservice.GetDataGrid("GetPatientsChatsPerDoctor").then(result => {
			if (result) {
				let patientobj = result.find((item) => item.lowUnreadedMsg == true);
				this.setState({
					patientList: result || [],
					isLoading: false,
					patient: patientobj
				})

				Dataservice.GetDataGrid("GetChatBetweenPatientAndDoctor?patientId=" + patientobj.patientId).then(res => {
					this.setState({
						messagesList: res.reverse() || [],
					})
				});
			}
		});
		document.body.classList.add('chat-page');

		this.connection.onclose(async () => {
			await this.start();
		});

		this.connection.on('ReceiveMessage', (message) => {
			this.mapReceivedMessage(message);
		});

		this.start();
	}

	async start() {
		try {
			await this.connection.start();
			console.log('connected');
		} catch (err) {
			setTimeout(() => this.start(), 5000);
		}
	}

	mapReceivedMessage(message) {
		if (message.patientId === this.state.patientId) {
			let messagesList = [...this.state.messagesList];

			messagesList.push(message);

			this.setState({ messagesList }, () => {
				let element = document.getElementById('messages');

				element.scrollTop = element.scrollHeight - element.clientHeight;
			});
		}
	}

	onKeyUp(event) {
		if (event.charCode === 13) {
			this.sendMessage();
		}
	}

	scrollToBottom(id) {
		const scrollHeight = this.messageList.scrollHeight;
		const height = this.messageList.clientHeight;
		const maxScrollTop = scrollHeight - height;
		this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
	}

	componentWillUnmount() {
		document.body.classList.remove('chat-page');
	}

	handleChatClick = (ptientId) => {
		this.setState({
			patientsLoading: true,
			activeClass:true
		})

		Dataservice.GetDataGrid("GetChatBetweenPatientAndDoctor?patientId=" + ptientId).then(result => {
			this.setState({
				messagesList: result.reverse() || [],
				isLoading: false
			})
			Dataservice.GetDataGrid("GetPatientsChatsPerDoctor").then(res => {
				this.setState({
					patientList: res || [],
					patientsLoading: false
				})
			});
		});

		let patientobj = this.state.patientList.find((item) => item.patientId == ptientId);

		this.setState({
			chatSlide: !this.state.chatSlide,
			patient: patientobj
		})
	}

	sendMessage = (values) => {
		this.setState({
			chatLoading: true
		});
		if (this.state.chatobj.subject) {
			var sendMsg = {
				subject: this.state.chatobj.subject,
				patientId: this.state.patient.patientId,
				doctorId: window.localStorage.getItem('doctorId'),
				isPatient:false
			};
			let copyMessages = [...this.state.messagesList]
			Dataservice.sendMessage(sendMsg).then((res) => {
				copyMessages.push(sendMsg);
				this.setState({
					messagesList: copyMessages || [],
					chatLoading: false,
					chatobj: {
						subject: ''
					}
				}, () => this.scrollToMyRef())
			})
		}
	};

	getMessages = (obj) => {
		let url = '';

		this.setState({
			influencerId: obj.id,
			isLoading: true,
		});

		url = 'GetChattingMessages?influncerId=' + obj.id;

		if (obj.id > 0) {
			Dataservice.GetDataGrid(url).then((res) => {
				if (res != null) {
					let profileTalk =
						res.length > 0
							? res[0].influenerProfilePic
								? config.hubUrl.replace('/Doctor', '') + res[0].influenerProfilePic
								: profile
							: profile;

					let messageIds = [];

					res.forEach((item) => {
						messageIds.push(item.id);
					});

					Dataservice.addObject('updateMessagesStatus', messageIds);

					let url = 'GetClientUnreadMessageAndNotification';

					this.props.actions.changeNotificationMessageCounter(url);

					this.setState(
						{
							messagesList: reverse(res) || [],
							influncerName:
								res.length > 0 ? res[0].influenerName : '',
							profileTalk: profileTalk,
							isLoading: false,
							reqStatus: res != null ? res[0].reqStatus : null,
						},
						() => {
							let element = document.getElementById('messages');

							element.scrollTop =
								element.scrollHeight - element.clientHeight;
						},
					);
				}
			});
		}
	};
	chathandleChange = (e) => {
		let chatobj = this.state.chatobj;
		chatobj.subject = e.target.value;
		this.setState({
			chatobj
		})
	}
	scrollToMyRef = () => {
		const scroll =
			this.chatContainer.current.scrollHeight -
			this.chatContainer.current.clientHeight;
		this.chatContainer.current.scrollTo(0, scroll);
	};
	render() {
		return (
			this.state.isLoading ? <LoadingSection /> :
				< >
					<div className="row">
						<div className="col-xl-12">
							<div className={this.state.chatSlide ? "chat-window   chat-slide " : 'chat-window '}   >
								<div className="chat-cont-left">
									<div className="chat-header">
										<span>Chats</span>
										<a href="#0" className="chat-compose" style={{ visibility: "hidden" }}>
											<i className="material-icons">control_point</i>
										</a>
									</div>
									<form className="chat-search">
										<div className="input-group">
											<div className="input-group-prepend">
												<i className="fas fa-search"></i>
											</div>
											<input type="text" className="form-control" placeholder="Search" />
										</div>
									</form>
									<div className="chat-users-list">
										<div className="chat-scroll">
											{
												this.state.patientsLoading ? <LoadingSection /> :
													this.state.patientList.map(ptient => {
														return (
															<a href="#0" key={ptient.id} onClick={event => this.handleChatClick(ptient.patientId)} 
															className={this.state.activeClass==true && this.state.patient.id==ptient.id?"media active":"media"}>
																<div className="media-img-wrap">
																	<div className="avatar avatar-away">
																		<img src={ptient.patientPhoto == null ? Avatar : config.hubUrl.replace('/Doctor', '') + ptient.patientPhoto}
																			alt="User" className="avatar-img rounded-circle" />
																	</div>
																</div>
																<div className="media-body">
																	<div>
																		<div className="user-name">{ptient.patientName}</div>
																		<div className="user-last-chat">{ptient.subject}</div>
																	</div>
																	<div>

																		<div className="last-chat-time block">{moment(ptient.sendDate).format('DD MMM YYYY')}</div>
																		<div className="badge badge-success badge-pill">{ptient.unreadedMsgCount == 0 ? "" : ptient.unreadedMsgCount}</div>
																	</div>
																</div>
															</a>

														)
													})}

										</div>
									</div>
								</div>

								<div className="chat-cont-right">
									<div className="chat-header">
										<a id="back_user_list" onClick={this.handleChatClick} href="#0" className="back-user-list">
											<i className="material-icons">chevron_left</i>
										</a>
										<div className="media">
											<div className="media-img-wrap">
												<div className="avatar avatar-online">
													<img src={this.state.patient.patientPhoto == null ? Avatar : config.hubUrl.replace('/Doctor', '') + this.state.patient.patientPhoto}
														alt="User" className="avatar-img rounded-circle" />
												</div>
											</div>
											<div className="media-body">
												<div className="user-name">{this.state.patient.patientName}</div>
												<div className="user-status">online</div>
											</div>
										</div>
										<div className="chat-options">
											<a href="#0" data-toggle="modal" data-target="#voice_call" onClick={() => this.openModal('voice')}>
												<i className="material-icons">local_phone</i>
											</a>
											<a href="#0" data-toggle="modal" data-target="#video_call" onClick={() => this.openModal('video')}>
												<i className="material-icons">videocam</i>
											</a>
											<a href="#0">
												<i className="material-icons" style={{ visibility: "hidden" }}>more_vert</i>
											</a>
										</div>
									</div>
									<div className="chat-body" style={{ position: 'relative' }}>
										{this.state.chatLoading == true ? <LoadingSection position="absolute" /> :
											<div className="chat-scroll" ref={this.chatContainer}>
												<ul className="list-unstyled">
													{this.state.messagesList.map(msg => {
														if (msg.isPatient == true )
														return (
															<li className="media received" key={msg.id}>
																<div className="avatar">
																	<img src={this.state.patient.patientPhoto == null ? Avatar : config.hubUrl.replace('/Doctor', '') + this.state.patient.patientPhoto}
																		alt="User" className="avatar-img rounded-circle" />
																</div>
																<div className="media-body">
																	<div className="msg-box">
																		<div>
																			<p>{msg.subject}</p>
																			<ul className="chat-msg-info">
																				<li>
																					<div className="chat-time">
																						<span>{moment(msg.sendDate).format('DD MMM YYYY')}</span>
																					</div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	{msg.chatFiles == null ? "" :
																		<div className="msg-box">
																			<div>
																				<div className="chat-msg-attachments">
																					<div className="chat-attachment">
																						<img src={config.hubUrl.replace('/Doctor', '') + msg.chatFiles.fileUrl} alt="Attachment" />
																						{/* <div className="chat-attach-caption">placeholder.jpg</div> */}
																						<a href="#0" className="chat-attach-download">
																							<i className="fas fa-download"></i>
																						</a>
																					</div>

																				</div>

																			</div>
																		</div>
																	}
																</div>
															</li>
														)
														else
															return (
																<li className="media sent" key={msg.id}>
																	<div className="media-body">
																		<div className="msg-box">
																			<div>
																				<p>{msg.subject}</p>
																				<ul className="chat-msg-info">
																					<li>
																						<div className="chat-time">
																							<span>{moment(msg.sendDate).format('DD MMM YYYY')}</span>
																						</div>
																					</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</li>
															)
													})}

												</ul>
											</div>
										}
									</div>
									<div className="chat-footer">
										<Formik
											initialValues={{
												...this.state.chatobj,
											}}
											onSubmit={values => {
												if (!this.state.isLoading)
													this.sendMessage(values);
											}}>
											{({ errors, touched, handleBlur, handleChange }) => (
												<Form>
													<div className="input-group">
														<div className="input-group-prepend">
															<div className="btn-file btn">
																<i className="fa fa-paperclip"></i>
																<input type="file" />
															</div>
														</div>
														<input type="text"
															className="input-msg-send form-control"
															name="subject"
															onBlur={handleBlur}
															onChange={e => this.chathandleChange(e)}
															placeholder="Type something"
															value={this.state.chatobj.subject}
														/>
														<div className="input-group-append">
															<button type="submit" className="btn msg-send-btn" ><i className="fab fa-telegram-plane"></i></button>
														</div>
													</div>
												</Form>
											)}
										</Formik>

									</div>
								</div>


							</div>
						</div>
					</div>

					{/* modal for video*/}
					<Modal show={this.state.activeModal === 'video'} onHide={this.handleCloseModal} centered>
						<Modal.Body>
							<div className="call-box incoming-box">
								<div className="call-wrapper">
									<div className="call-inner">
										<div className="call-user">
											<img alt="User" src={IMG01} className="call-avatar" />
											<h4>Dr. Darren Elder</h4>
											<span>Connecting...</span>
										</div>
										<div className="call-items">
											<a href="#0" className="btn call-item call-end" data-dismiss="modal" aria-label="Close">
												<Icon>call_end</Icon>
											</a>
											<Link to="/voice-call" className="btn call-item call-start"><i className="material-icons">call</i></Link>
										</div>
									</div>
								</div>
							</div>
						</Modal.Body>
					</Modal>
					{/* modal for call*/}
					<Modal show={this.state.activeModal === 'voice'} onHide={this.handleCloseModal} centered>
						<Modal.Body>
							<div className="call-box incoming-box">
								<div className="call-wrapper">
									<div className="call-inner">
										<div className="call-user">
											<img alt="User" src={IMG01} className="call-avatar" />
											<h4>Dr. Darren Elder</h4>
											<span>Connecting...</span>
										</div>
										<div className="call-items">
											<a href="#0" className="btn call-item call-end" data-dismiss="modal" aria-label="Close">
												<Icon>call_end</Icon>
											</a>
											<Link to="/voice-call" className="btn call-item call-start"><i className="material-icons">call</i></Link>
										</div>
									</div>
								</div>
							</div>
						</Modal.Body>
					</Modal>

				</ >

		);
	}
}
export default DoctorChat;